

var userName = "";
var password = "";
var vpwd = "";
var phone = "";
var bday = "";
var acctType = "";
var deviceid = "";
var form = "";
var elements = "";

function setusername(){
    userName = $("#un").val();
}

function setuserpassword(){
    password = $("#pw").val();
}

function setvpwd(){
    vpwd = $("#vpwd").val();
}

function setemail(){
    email = $("#email").val();
}

function setphone(){
    phone = $("#phone").val();
}

function setbday(){
    bday = $("#bday").val();
}

function setaccttype(){
    acctType = $('input[name="accttype"]:checked').val();
}

function setdeviceid(){
     deviceid = $("#deviceid").val();
}

function savetoken(token){
// whatever passes as token should save into local storage
    if (window.localStorage){
     localStorage.setItem("token", token);
    }

}

function checkexpiredtoken(){
// read token from local storage - check with ajax call
    if(window.localStorage){
    usertoken = localStorage.getItem("token", token);
    $.ajax({
       type: 'GET',
        url: '/checkToken',
        data: '{"usertoken":"' + usertoken + '"}',
        success: function(data){savetoken(data)},
        contentType: "application/text",
        dataType: 'text' })
    }
}

function userlogin(){
    setuserpassword();
    setusername();
    $.ajax({
        type: 'POST',
        url: '/login',
        data: ' {"userName":"'+ userName +'", "password":"'+ password +'"}', // or JSON.stringify ({name: 'jonas'}),
        success: function(data) {savetoken(data);
                    window.location.href = "/timer.html"
            },
        contentType: "application/text",
        dataType: 'text'
    });

}

function readonlyforms(formid){
    form = document.getElementById(formid);
    elements = form.elements;
    for (i = 0, len = elements.length; i < len; ++i) {
    elements[i].readOnly = true;
    }
    createbutton();
}
 function pwsDisableInput( element, condition ) {
        if ( condition == true ) {
            element.disabled = true;

        } else {
            element.removeAttribute("disabled");
        }

 }

function createbutton(){
    var button = document.createElement("input");
    button.type = "button";
    button.value = "OK";
    button.onclick = window.location.href = "/index.html";
    context.appendChild(button);
}


function createuser(){
    setaccttype();
    $.ajax({
        type: 'POST',
        url: '/createuser',
        data: '{"userName":"'+ userName +'", "password":"'+ password + '", "verifyPassword":"' + vpwd + '", "email":"' + email +
                '", "phone":"' + phone + '", "birthDay":"' + bday + '", "deviceNickName":"'+ deviceid + '", "accountType":"Personal"}',
        success: function(data) { alert(data);
//        readonlyforms("newUser");
//        alert(readonlyforms("newUser"));
        window.location.href = "/index.html"},
        contentType: "application/text",
        dataType: 'text'
    });
}

function getstephistory(){
      $.ajax({
            type: 'POST',
            url: '/stephistory',
            data: '{"userName":"'+ userName +'"}',
            success: function(data) { alert(data);
            json = $.parseJSON(data);
            $('#results').html(json.name+' Total Steps: ' + json.stepTotal)},
            contentType: "application/text",
            dataType: 'text'
        });
}