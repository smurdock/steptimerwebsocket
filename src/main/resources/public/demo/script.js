    var clicks = 0;
    var stopwatch;
    var runningstate = 0; // 1 means the timecounter is running 0 means counter stopped
    var stoptime = 0;
    var lapcounter = 0;
    var currenttime;
    var lapdate = '';
//    var userToken = getUrlParam
    var webSocket  = new WebSocket("ws://"+location.hostname+":"+location.port+"/socket/");
    webSocket.onmessage = function (msg) {updateStepCount(msg);};
    webSocket.onopen = function (event){webSocket.send("StartDemo")};
    webSocket.onclose = function () {alert("Thank you for visiting Sure Steps, your session has ended")};

    function updateStepCount(msg){
        if(msg.data=="startTimer"){
            startandstop();
        } else if (msg.data.indexOf("stepCount")>-1){
            onClick();
        }
    }

    function onClick() {
    	if(clicks=="Test Complete"){
    	    clicks=0;
    	}
        clicks += 1;
        if(clicks==30){
        	clicks="Test Complete";
        	startandstop();
        }
        document.getElementById("clicks").innerHTML = clicks;
	 };

    function timecounter(starttime)
        {
        currentdate = new Date();
                stopwatch = document.getElementById('stopwatch');
         
        var timediff = currentdate.getTime() - starttime;
        if(runningstate == 0)
            {
            timediff = timediff + stoptime
            }
        if(runningstate == 1)
            {
            stopwatch.value = formattedtime(timediff);
            refresh = setTimeout('timecounter(' + starttime + ');',10);
            }
        else
            {
            window.clearTimeout(refresh);
            stoptime = timediff;
            }
        }
 

function startandstop()
      {
      var startandstop = document.getElementById('startandstopbutton');
      var startdate = new Date();
      var starttime = startdate.getTime();
      if(runningstate==0)
    {
      startandstop.value = 'Stop';
      runningstate = 1;
      timecounter(starttime);
     }
 else
      {
        startandstop.value = 'Start';
        runningstate = 0;
        lapdate = '';
      }
   }
      function resetstopwatch()
        {
            lapcounter = 0;
       stoptime = 0;
      lapdate = '';
      window.clearTimeout(refresh);
      
     if(runningstate == 1)
   {
   var resetdate = new Date();
   var resettime = resetdate.getTime();
   timecounter(resettime);
   
  }
else
  {
stopwatch.value = "0:0:0";
document.getElementById("clicks").innerHTML = 0;
  }
 }
 function formattedtime(unformattedtime)
  { 
   var decisec = Math.floor(unformattedtime/100) + '';
   var second = Math.floor(unformattedtime/1000);
    var minute = Math.floor(unformattedtime/60000);
decisec = decisec.charAt(decisec.length - 1);
second = second - 60 * minute + '';
return minute + ':' + second + ':' + decisec;
}