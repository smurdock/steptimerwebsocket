package com.getsimplex.steptimer.service;

/**
 * Created by sean on 8/10/2016 based on https://github.com/tipsy/spark-websocket/tree/master/src/main/java
 */


import com.getsimplex.steptimer.model.User;
import com.getsimplex.steptimer.utils.*;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.util.Optional;

import static spark.Spark.*;

public class WebAppRunner {

    public static void main(String[] args){

        Spark.port(Configuration.getConfiguration().getInt("suresteps.port"));
		//secure("/Applications/steptimerwebsocket/keystore.jks","password","/Applications/steptimerwebsocket/keystore.jks","password");
        staticFileLocation("/public");
        webSocket("/socket", WebSocketHandler.class);
        post("/sensorUpdates", (req, res)-> WebServiceHandler.routeDeviceRequest(req));
        post("/generateHistoricalGraph", (req, res)->routePdfRequest(req, res));
        get("/readPdf", (req, res)->routePdfRequest(req, res));
        get ("/stephistory", (req, res)-> getStepHistory(req));
        post("/createuser", (req, res)-> callUserDatabase(req));
        post("/login", (req, res)->loginUser(req));
        post("/saverapidsteptest", (req, res)->saveStepSession(req));
        get("/checkToken",((req, res) -> tokenCheck(req) ));
        get("/riskScore",((req,res) -> riskScore(req)));
        post ("/sensorTail",(req,res) -> saveTail(req,res) );

        init();
    }

    public static String saveTail(Request req, Response res) throws Exception{
        return StepHistory.saveSensorTail(req);
    }

    public static String routePdfRequest(Request req, Response res) throws Exception{
        return WebServiceHandler.routePdfRequest(req, res);
    }

    private static String callUserDatabase(Request request)throws Exception{
        return CreateNewUser.handleRequest(request);
    }

    private static String loginUser(Request request) throws Exception{
        return LoginController.handleRequest(request);

    }
    private static String getStepHistory(Request request) throws Exception{
        return StepHistory.handleRequest(request);

    }

    private static String riskScore(Request request) throws Exception{
        String userName = authenticateSession(request);
        return StepHistory.riskScore(userName);
    }

    private static String saveStepSession(Request request) throws Exception{
        return SaveNewRapidStepTest.handleRequest(request);

    }

    private static String tokenCheck(Request request) throws Exception{
        return TokenController.handleRequest(request);
    }

    public static String authenticateSession(Request request) throws Exception{

        String tokenString = request.headers("suresteps.session.token");

        Optional<User> user = TokenService.getUserFromToken(tokenString);//

        Boolean tokenExpired = SessionValidator.validateToken(tokenString);

        if (!user.isPresent()){
            throw new Exception("Could not find user with token");
        }
        else if(tokenExpired.equals(true)){ //Check to see if session expired
            throw new Exception("Session Expired");
        }else {
            return user.get().getUserName();
        }

    }



}
