package com.getsimplex.steptimer.service;

import com.google.gson.Gson;
import com.getsimplex.steptimer.model.RapidStepTest;
import com.getsimplex.steptimer.model.NewRapidStepTest;
import spark.Request;
import com.getsimplex.steptimer.utils.GsonFactory;
import com.getsimplex.steptimer.utils.JedisData;

import java.util.ArrayList;
import java.util.Date;

import static com.getsimplex.steptimer.service.TokenController.expirationTest;

/**
 * Created by sean on 9/7/2016.
 */
public class SaveNewRapidStepTest {
    private static Gson gson = GsonFactory.getGson();


    public static String handleRequest(Request request) throws Exception{

        String startNewTestString = request.body();
        NewRapidStepTest newRapidStepTest = gson.fromJson(startNewTestString, NewRapidStepTest.class);
        RapidStepTest rapidStepTest = new RapidStepTest();

        String userName = newRapidStepTest.getUserName();
        Boolean sessionExpired = expirationTest(userName);

        if(sessionExpired.equals(false)){ //Check to see if session has expired
            return "Please Login";
        }else {
            rapidStepTest.setUserName(userName);

            Date startTest = newRapidStepTest.getStartTime();
            rapidStepTest.setStartTime(startTest);

            ArrayList stepPoints = newRapidStepTest.getStepPoints();
            rapidStepTest.setStepPoints(stepPoints);

            Integer totalSteps = newRapidStepTest.getTotalSteps();
            rapidStepTest.setTotalSteps(totalSteps);

            Integer feelingScore = newRapidStepTest.getFeelingScore();
            rapidStepTest.setFeelingScore(feelingScore);


            //SAVE New Test Results TO REDIS
            if (rapidStepTest != null) {
                JedisData.loadToJedis(rapidStepTest, NewRapidStepTest.class);
                return "Step Session Saved";
            } else {
                throw new Exception("Step Session did not save");
            }
        }
    }
}
