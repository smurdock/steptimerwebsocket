package com.getsimplex.steptimer.service;

import com.getsimplex.steptimer.model.LoginToken;
import com.getsimplex.steptimer.model.Token;
import com.getsimplex.steptimer.model.User;
import com.getsimplex.steptimer.utils.JedisData;
import com.google.gson.Gson;
import spark.Request;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import static com.getsimplex.steptimer.utils.JedisData.deleteFromRedis;

/**
 * Created by Administrator on 12/7/2016.
 */
public class TokenController {

    private static Gson gson = new Gson();


    public static String handleRequest(Request request) throws Exception{
        String checkTokenString = request.body();
        Token newToken = gson.fromJson(checkTokenString, Token.class);
        return tokenLookupToken(newToken.getUuid());

    }

//    private static LoginRequest loginRequest = new LoginRequest();
//    private static String userName = loginRequest.getUserName();

    private static ArrayList<User> allUsers = JedisData.getEntityList(User.class);

    public static void tokenSearch(String userName) throws Exception{

        Predicate<User> userPredicate = user -> user.getUserName().equals(userName);
        Optional<User> userOptional = allUsers.stream().filter(userPredicate).findFirst();

        if (userOptional.isPresent()) {
        //create logintoken for user - save in redis
        ArrayList<LoginToken> allTokens = JedisData.getEntityList(LoginToken.class);
        Predicate<LoginToken> tokenPredicate = token -> token.getUser().equals(userName);

        if(tokenLookupUN(userName).isEmpty()){
            createUserToken(userName);
        }else if (!tokenLookupUN(userName).isEmpty()){
            Predicate<LoginToken> activePredicate = active -> active.getExpires() && active.getExpiration().after(new Date());
            Optional<LoginToken> tokenOptional = allTokens.stream().filter(tokenPredicate).filter(activePredicate).findFirst();
            LoginToken loginToken = tokenOptional.get();

//          recreates (resets) active token
            deleteFromRedis(loginToken);
            createUserToken(userName);
        }

        System.out.println(tokenLookupUN(userName));

    }else {
        throw new Exception("User not found");
    }
}

    public static Boolean expirationTest(String userName) throws Exception{
        ArrayList<LoginToken> allTokens = JedisData.getEntityList(LoginToken.class);
        Predicate<LoginToken> tokenPredicate = token -> token.getUser().equals(userName);

        Optional<LoginToken> tokenOptional = allTokens.stream().filter(tokenPredicate).findFirst();
        if (tokenOptional.isPresent()){
            LoginToken loginToken = tokenOptional.get();
            if(loginToken.getExpires() && loginToken.getExpiration().before(new Date()) ){
                return false; //Session has expired
            }else{
                return true; //Session not expired
            }
        }else {
            return false; //Session has expired
        }
    }

    public static String tokenLookupUN(String userName)throws Exception {
        ArrayList<LoginToken> allTokens = JedisData.getEntityList(LoginToken.class);
        Predicate<LoginToken> tokenPredicate = token -> token.getUser().equals(userName);
        Predicate<LoginToken> expirePredicate = expire -> expire.getExpires() && expire.getExpiration().before(new Date());
        Predicate<LoginToken> activePredicate = active -> active.getExpires() && active.getExpiration().after(new Date());

        //        Deletes all expired tokens
        List<LoginToken> expiredTokens = allTokens.stream().filter(tokenPredicate).filter(expirePredicate).collect(Collectors.toList());
        if (!expiredTokens.isEmpty()) {
            deleteFromRedis(expiredTokens);
        }

//        finds active token and returns it
        Optional<LoginToken> tokenOptional = allTokens.stream().filter(tokenPredicate).filter(activePredicate).findFirst();
        if (tokenOptional.isPresent()) {
            LoginToken loginToken = tokenOptional.get();
            return loginToken.getUuid();
        } else {
            return "";

        }

    }
    public static String tokenLookupToken(String testToken)throws Exception {
        ArrayList<LoginToken> allTokens = JedisData.getEntityList(LoginToken.class);
        Predicate<LoginToken> tokenPredicate = token -> token.getUuid().equals(testToken);
        Predicate<LoginToken> expirePredicate = expire -> expire.getExpires() && expire.getExpiration().before(new Date());
        Predicate<LoginToken> activePredicate = active -> active.getExpires() && active.getExpiration().after(new Date());

        //        Deletes all expired tokens
        List<LoginToken> expiredTokens = allTokens.stream().filter(tokenPredicate).filter(expirePredicate).collect(Collectors.toList());
        if (!expiredTokens.isEmpty()) {
            deleteFromRedis(expiredTokens);
        }

//        finds active token and returns it
        Optional<LoginToken> tokenOptional = allTokens.stream().filter(tokenPredicate).filter(activePredicate).findFirst();
        if (tokenOptional.isPresent()) {
            LoginToken loginToken = tokenOptional.get();
            return loginToken.getUuid();
        } else {
            return "";

        }

    }

    public static String createUserToken(String userName)throws Exception{
        ArrayList<User> allUsers = JedisData.getEntityList(User.class);
        Predicate<User> userPredicate = user -> user.getUserName().equals(userName);
        Predicate<User> personalTypePredicate = personal -> personal.getAccountType().equals("personal");
//        Predicate<User> businessTypePredicate = business -> business.getAccountType().equals("Business");

        Optional<User> personalOptional = allUsers.stream().filter(userPredicate).filter(personalTypePredicate).findFirst();
//        Optional<User> businessOptional = allUsers.stream().filter(userPredicate).filter(businessTypePredicate).findFirst();

        String tokenString = UUID.randomUUID().toString();
        Long currentTimeMillis = System.currentTimeMillis();
        LoginToken token = new LoginToken();
        token.setExpires(true);
        token.setUuid(tokenString);
        token.setUser(userName);

        if (personalOptional.isPresent()) {
            Long expiration = currentTimeMillis + 10 * 60 * 1000;  // expires after 10 minutes
            Date expirationDate = new Date(expiration);
            token.setExpiration(expirationDate);

        }else{
            Long expiration = currentTimeMillis + 60 * 60 * 1000;  // expires after 1 hours (business account)
            Date expirationDate = new Date(expiration);
            token.setExpiration(expirationDate);
        }
        JedisData.loadToJedis(token, LoginToken.class);
        return tokenString;

    }
}
