package com.getsimplex.steptimer.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Administrator on 10/17/2016.
 */
public class NewRapidStepTest {
    private String userName;
    private Date startTime;
    private ArrayList<Double> stepPoints;
    private Date stopTime;
    private Integer totalSteps;
    private Integer feelingScore;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public ArrayList getStepPoints() {
        return stepPoints;
    }

    public void setStepPoints(ArrayList stepPoints) {
        this.stepPoints = stepPoints;
    }

    public Date getStopTime() {
        return stopTime;
    }

    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }

    public Integer getTotalSteps() {
        return totalSteps;
    }

    public void setTotalSteps(Integer totalSteps) {
        this.totalSteps = totalSteps;
    }

    public Integer getFeelingScore() {
        return feelingScore;
    }

    public void setFeelingScore(Integer feelingScore) {
        this.feelingScore = feelingScore;
    }
}
